/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.midalgo_a1_8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author WIP
 */
public class ExamA1_8 {

    public static void main(String[] args) {
        long start, stop;

        Scanner kb = new Scanner(System.in);

        ArrayList<Integer> arrNum = new ArrayList<>();
        ArrayList<Integer> arrReverse = new ArrayList<>();
        System.out.println("Input 5 number : ");
        //input
        for (int i = 0; i < 5; i++) {
            int num = kb.nextInt();
            arrNum.add(num);
        }

        start = System.nanoTime();
        //process O(1)
        for (int i = arrNum.size() - 1; i >= 0; i--) {
            arrReverse.add(arrNum.get(i));
        }
        
        System.out.println(arrReverse);

        stop = System.nanoTime();
        System.out.println("time = " + (stop - start) * 1E-9 + "secs.");

    }
}
